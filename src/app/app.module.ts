/// <reference path="../../bower_components/dt-angular/angular.d.ts"/>
/**
 * Created by jonathanrobic on 27/01/15.
 */

((): void => {
  'use strict';

  angular
    .module('app', [
      'app.core',
      'app.layout',
      'app.services',
      'app.widgets',
      'app.blocks',
    /**
     * Features areas
     */
      'app.blogposts',
      'app.dashboard',
      'app.sitesettings',
      'app.users',
      'app.usersettings'
    ]);

})();
