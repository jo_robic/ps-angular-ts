/// <reference path="../../../bower_components/dt-angular/angular-route.d.ts"/>
/**
 * Created by jonathanrobic on 27/01/15.
 */

((): void => {
	'use strict';

  angular
    .module('app.blogposts')
    .config(config);

  config.$inject = ['$routeProvider'];

  function config($routeProvider: ng.route.IRouteProvider): void {
    $routeProvider
      .when('/admin/blogposts/newposts/newpost', {
        template: '<div>NewPost</div>',
        controller: (): void => { },
        controllerAs: 'vm'
      });
  }

})();
