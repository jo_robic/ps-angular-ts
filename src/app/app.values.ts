/**
 * Created by jonathanrobic on 27/01/15.
 */

interface ICurrentUser {
  userId: string;
  fullName: string;
}

((): void => {
	'use strict';

  var currentUser: ICurrentUser = {
    userId: '',
    fullName: ''
  };

  angular
    .module('app')
    .value('currentUser', currentUser)

})();
