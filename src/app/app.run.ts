/// <reference path="../../bower_components/dt-angular/angular-cookies.d.ts"/>
/**
 * Created by jonathanrobic on 27/01/15.
 */

interface IAppCookies extends ng.cookies.ICookiesService {
  userId: string;
}

((): void => {
	'use strict';

  angular
    .module('app')
    .run(run);

  run.$inject = [
    '$rootScope',
    '$cookies',
    'currentUser'
  ];

  function run($rootScope: ng.IRootScopeService, $cookies: IAppCookies, currentUser: ICurrentUser): void {
    $rootScope.$on('$routeChangeError', (): void => {

    });

    currentUser.userId = $cookies.userId;
  }
})();
